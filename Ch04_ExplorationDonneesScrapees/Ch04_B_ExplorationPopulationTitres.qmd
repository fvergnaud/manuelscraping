---
# title: "Explorer les titres des articles de Population au moyen de la statistique textuelle"
# author: "B. Garnier"
# date: "today"
# format:
#   html:
#     toc: true
#     toc-depth: 3
#     theme: zephyr
#     highlight-style: github
#     code-block-bg: true
#     code-block-border-left: black
#     self-contained: true
# editor: 
#   markdown: 
#     wrap: sentence
# execute: 
#   warning: false
#   message: false
#   echo: true
#   eval: true
---

# Statistiques textuelles sur les titres d'articles de la revue Population entre 1946 et 2020 avec R.temis {#sec-mise-en-application-R.temis .unnumbered}

Dans cette partie, nous illustrons l'application de la [statistique textuelle](http://ses-perso.telecom-paristech.fr/lebart/ST.html) sur les données scrapées du site de la [revue Population](http://www.revue-population.fr/).

La partie précedente nous a permis de sélectionner les *articles ou assimilés* les plus comparables possible ; ici nous allons *explorer* les *champs lexicaux* et rechercher les *spécificités* des titres dans le temps ou selon les auteurs.

Nous utilisons le package [*R.temis*](http:/rtemis.hypotheses.org) de R qui permet de mettre en oeuvre les étapes essentielles de l'analyse textuelle :

-   importation de corpus stocké dans un tableau (.csv) ou dans un texte (.txt),
-   création de tableau lexical (*Document Text Matrix*),
-   recherche de concordances,
-   suppression de mots vides,
-   lemmatisation,
-   calcul d'occurrences,
-   calcul de spécificités,
-   détection de cooccurrences,
-   analyse des correspondances, classification,
-   affichage de nuage de mots, de graphes de mots

Ce package s'appuie sur les packages existants : *tm*, *FactoMineR*, *explor*, *igraph*.

[tm](https://cran.r-project.org/web/packages/tm/index.html) est le premier package R créé pour des applications de fouille de texte.

[FactoMineR](http://factominer.free.fr/index_fr.html) est dédié à l'analyse géométrique des données (Analyses factorielles, classifications).

[Explor](https://cran.r-project.org/web/packages/explor/vignettes/introduction_fr.html) facilite l'*exploration interactive* des résultats d'une analyse exploratoire multidimensionnelle (aides à l'interprétation et plan factoriels).

[igraph](https://r.igraph.org/) est dédié à l'analyse des données relationnelles (graphes de réseaux).


Nous n'utilisons pas de packages supplémentaires qui auraient permis d'améliorer les sorties (tableaux statistiques ou graphiques) afin de montrer des scripts les plus simples possibles où l'on ne paramètre que les calculs de statistiques textuelles.

On aura besoin des packages suivants :

```{r paquets, message=FALSE, warning=FALSE}
  # Statistiques textuelles
library(R.temis)
library (dplyr)
library (ggplot2)
```

Il s'agit en premier lieu d'importer le fichier (.csv) contenant les 4247 titres des articles ou assimilés en français retenus.

Pour chacune, nous disposons des informations suivantes (colonnes du fichier) :

-   ident : identifiant de la publication
-   titre : titre de la publication,
-   Annee : année de publication (de 1946 à 2020),
-   Periode : Décennie de la publication,
-   auteur_nbs : nombre d'auteurs de la publication (de 1 à 4 et plus),
-   rubrique_net2 : type de rubrique retravaillé *(pas toujours renseigné)*.

Les *titres* sont composés de quelques mots, on va considérer cette donnée comme une *variable textuelle* et les autres variables comme des *métadonnées*.

La fonction `import_corpus` permet d'importer le *corpus* et les *métadonnées* associées dans r.temis.

```{r Import_titres}
# On crée le corpus avec la 2e colonne du tableau au format csv puisque c'est là que sont les titres et on précise que les textes sont en français.
corpus <- import_corpus("Ch04_Data/corpus_tit_fra.csv", format="csv",textcolumn = 1, language="fr")
```

## Premier tableau lexical associé aux titres

### Lexique sans mots outils ni chiffres

La fonction `build_dtm` transforme le corpus en **tableau lexical** (*Document Term Matrix* de r).
C'est un tableau comportant en lignes les unité textuelles (ici les titres) et en colonnes les différents mots utilisés dans les titres.
Les cases de ce tableau sont essentiellement remplies de 0, sauf quand le mot est présent dans le titre.

```{r dtm1}
# On choisit ici de supprimer les mots-outils et les chiffres car on ne souhaite pas les utiliser dans les analyses.
dtm_t <- build_dtm(corpus, remove_stopwords = TRUE, remove_numbers = TRUE)
dtm_t
```

Les mots de une lettre sont également éliminés automatiquement.

Les lignes du *tableau lexical* (ici dtm_t) correspondent ici aux 4247 titres en français et les colonnes aux 4893 mots différents utilisés dans les titres (hors mots-outils et chiffres).

### Afficher des extraits des données importées

```{r voir les donnée}
# les titres 
head(sapply(corpus,as.character))
# les métadonnées
head(meta(corpus))
# le Tableau lexical
inspect(dtm_t)
```

### Afficher les mots les plus fréquents

```{r}
# Les 20 mots les plus fréquents dans les titres
frequent_terms(dtm_t, n=20)
```

Le mot *population*" est utilisé 596 fois dans les 4247 titres et représente 2,5 % des occurrences.

La fonction `dictionary` crée le *lexique* associé au corpus (ici dic_t).
Il est possible alors d'afficher ce lexique pour l'explorer (par ordre de fréquence ou alphabétique) et repérer que r a associé à chaque *term* sa racine (ou *token*) automatiquement.

```{r lex_net1}
dic_t <- dictionary(dtm_t)
head(dic_t)
#View(dic_t)
```

### Suppression de mots du lexique (au choix)

Un premier *lexique* qui rassemble les mots distincts du corpus des titres a été crée.
Il ne contient pas de chiffres ni de mots-outils.

Il est courant de devoir supprimer des mots qui ne semblent pas intéressant à analyser (dans notre cas, le mot "séance" apparaît 23 fois dans les titres).
Nous vérifions l'usage de ec mots dans notre corpus avec la fonction `concordances` qui permet, pour un mot choisi, d'afficher les unités textuelles (ici les titres) dans lesquelles il apparaît.

Affichage des concordances au mot "séance" :

```{r conc1}
concordances(corpus, dtm_t, "séance")
#concordances(corpus, dtm_t, "quelques")
#concordances(corpus, dtm_t, "xixe")
```

Nous pouvons enlever le mot "séance" de notre lexique.

Pour éliminer un ensemble de mots, écrivez l'ensemble de ces mots dans une liste (ici asupp) que vous supprimez du lexique (dic_t) pour un créer un nouveau qui ne contiendra pas ces mots.

```{r lex_net2}
asupp <- c("entre", "séance", "depuis","quelques")
                  
dic_t2 <- dic_t[!rownames(dic_t) %in% asupp,]
```

Les mots retenus pour l'analyse correspondent bien à un champs lexical de titres d'articles sur des questions de population.

### Lemmatiser

L'opération de *lemmatisation* permet de :

-   identifier les catégories grammaticales des mots,
-   regrouper des mots qui ont une signification proche : les verbes conjugués à la forme infinitive, les formes plurielles au singulier, les adjectifs du féminin au masculin
-   les mots sont alors appelés "Terms".

Cette opération permet de *réduire* le nombre de mots de lexique (Lebart, Salem).

Il peut être utile aussi de sélectionner pour l'analyse uniquement certaines catégories grammaticales de mots et donc de réduire encore la taille du lexique.

Pour notre exemple, nous utilisons un **lemmatiseur** crée à partir de [lexique 3](http://chrplr.github.io/openlexicon/datasets-info/Lexique382/README-Lexique.html) pour repérer la catégorie grammaticale de chaque mot de notre lexique associé au corpus de titres (dic_t2).

![](img/Ch05_lexique3.png){fig-align="center"}

Ce lemmatiseur adapté de Lexique 3, base de données lexicales du français contenant des représentations orthographiques et phonémiques, des lemmes associés ainsi que leur catégorie grammaticale ... La première colonne (ortho) contient les mots du lexique, la deuxième colonne le lemme associé à chaque mot, la troisième sa catégorie grammaticale (cgram), puis sa fréquence dans des corpus de livres (freqlemlivre, freqlemlivre2)) et dans des corpus de films (freqlemfilm, freqlemfilm2) ...

Celui-ci va nous permettre d'affecter la catégorie grammaticale des mots de notre lexique de titres.

Nous pourrons alors ne garder que les mots des catégories grammaticales qui nous intéressent pour l'analyse (ici les adverbes *ADV*, les verbes *VER*, les adjectifs *ADJ* et les noms *NOM*) ; nous gardons également les mots qui n'ont pas été identifiés grâce à notre lemmatiseur (liste *nr*).

A la lecture du lexique(dic_t), nous avons vu que la *racinisation de r* n'était pas optimale et nous allons pouvons l'améliorer grâce à notre lemmatiseur.

```{r lemm1}
# Importation du lexique dans le dataframe lexique3
lexique3 <- read.csv("Ch04_Data/Lexique383_simplifie.csv", fileEncoding="UTF-8")

# Tri de lexique3 par valeur décroissante de la colonne freqlivres pour que la valeur la plus fréquence du mot figure en premier
lexique3 <- arrange(lexique3, desc(freqlivres))

# suppression des lignes en doublon sur la base du mot (colonne ortho)  
lexique3 <- lexique3[!duplicated(lexique3$ortho),]

# voc_actif ne contiendra que les adverbes, verbes, adjectifs et noms propres de notre lexique 
voc_actif <- lexique3[lexique3$cgram %in% c("ADV", "VER", "ADJ", "NOM"),]

# Jointure entre les data frame dic_t2 et voc_actif.
# Les mots non reconnus par le lemmatiseur issu de lexique3 (cas de mots propres à certaines disciplines par exemple) sont conservés
dic_total_t <- merge(dic_t2, voc_actif, by.x="row.names", by.y="ortho", all.x=TRUE)

# Affecte à term la première valeur non manquante entre la valeur de la colonne lemme et de la colonne Term
dic_total_t <- mutate(dic_total_t, Term=coalesce(lemme, Term))
rownames(dic_total_t) <- dic_total_t$Row.names

# Lister les mots non reconnus par le lexique (pour vérification si besoin)  
#nr <- filter(dic_total_t, is.na(lemme))
```

### Correction ou complétion de la lemmatisation

La lemmatisation automatique a remplacé les *mots* par le *lemme* issu du lemmatiseur adapté de lexique 3.

Ainsi par exemple *france* a été remplacé par *franc*.
On peut également corriger des fautes de frappe ou d'orthographe.

Nous corrigons ce dictionnaire issu de la lemmatisation (dic_total_t) en en créant un autre (dicor) contenant dans une nouvelle colonne les mots *à corriger* (Term).

```{r lemm2}
dicor <- dic_total_t
# View(dicor)
dicor$Term[dicor$Term == "aages"] <- "ages"
dicor$Term[dicor$Term == "europ"] <- "europe"
dicor$Term[dicor$Term == "franc"] <- "france"
dicor$Term[dicor$Term == "étatsun"] <- "états_unis"
dicor$Term[dicor$Term == "étatsun"] <- "états_uniss"
dicor$Term[dicor$Term == "afriqu"] <- "afrique"
dicor$Term[dicor$Term == "ital"] <- "italie"
dicor$Term[dicor$Term == "alger"] <- "algérie"
dicor$Term[dicor$Term == "espagn"] <- "espagne"
dicor$Term[dicor$Term == "etud"] <- "étude"
dicor$Term[dicor$Term == "tunis"] <- "tunisie"
dicor$Term[dicor$Term == "démograph"] <- "démographe"
dicor$Term[dicor$Term == "ae"] <- "aes"
dicor$Term[dicor$Term == "asi"] <- "asie"
dicor$Term[dicor$Term == "pologn"] <- "pologne"
dicor$Term[dicor$Term == "hongr"] <- "hongrie"
dicor$Term[dicor$Term == "russ"] <- "russie"
dicor$Term[dicor$Term == "sahar"] <- "sahara"
dicor$Term[dicor$Term == "autrich"] <- "autriche"
dicor$Term[dicor$Term == "mexiqu"] <- "mexique"
dicor$Term[dicor$Term == "rouman"] <- "roumanie"
dicor$Term[dicor$Term == "dispar"] <- "	disparité"
dicor$Term[dicor$Term == "ancrag"] <- "ancrage"
dicor$Term[dicor$Term == "appari"] <- "appariement"
dicor$Term[dicor$Term == "antill"] <- "antilles"
dicor$Term[dicor$Term == "austral"] <- "australie"
dicor$Term[dicor$Term == "autonomis"] <- "autonomisation"
dicor$Term[dicor$Term == "bioéthiqu"] <- "bioéthique"
dicor$Term[dicor$Term == "biopolit"] <- "biopolitique"
dicor$Term[dicor$Term == "birman"] <- "birmanie"
dicor$Term[dicor$Term == "bretagn"] <- "bretagne"
dicor$Term[dicor$Term == "burkin"] <- "burkina"
dicor$Term[dicor$Term == "cherchel"] <- "cherchell"
dicor$Term[dicor$Term == "cinqu"] <- "cinquante"
dicor$Term[dicor$Term == "colomb"] <- "colombie"
dicor$Term[dicor$Term == "écrou"] <- "écroué"
dicor$Term[dicor$Term == "égypt"] <- "égypte"
dicor$Term[dicor$Term == "enquêt"] <- "enquête"
dicor$Term[dicor$Term == "érfi"] <- "erfi"
dicor$Term[dicor$Term == "etat"] <- "état"
dicor$Term[dicor$Term == "evolu"] <- "évolution"
dicor$Term[dicor$Term == "évolu"] <- "evolution"
dicor$Term[dicor$Term == "surmortal"] <- "surmortalité"
dicor$Term[dicor$Term == "gend"] <- "gender"
dicor$Term[dicor$Term == "gener"] <- "génération"
dicor$Term[dicor$Term == "génes"] <- "genèse"
dicor$Term[dicor$Term == "genr"] <- "genré"
dicor$Term[dicor$Term == "gg"] <- "ggs"
dicor$Term[dicor$Term == "guadeloup"] <- "guadeloupe"
dicor$Term[dicor$Term == "hor"] <- "hors"
dicor$Term[dicor$Term == "ile"] <- "île"
dicor$Term[dicor$Term == "immigrer"] <- "immigré"
dicor$Term[dicor$Term == "inégalit"] <- "inégalité"
dicor$Term[dicor$Term == "interact"] <- "interactif"
dicor$Term[dicor$Term == "island"] <- "islande"
dicor$Term[dicor$Term == "législ"] <- "législation"
dicor$Term[dicor$Term == "maf"] <- "mafe"
dicor$Term[dicor$Term == "martin"] <- "martinique"
dicor$Term[dicor$Term == "masculinis"] <- "masculinisation"
dicor$Term[dicor$Term == "mayott"] <- "mayotte"
dicor$Term[dicor$Term == "méthodolog"] <- "méthodologique"
dicor$Term[dicor$Term == "métropolis"] <- "métropolisation"
dicor$Term[dicor$Term == "mobil"] <- "mobilité"
dicor$Term[dicor$Term == "modeler"] <- "modèle"
dicor$Term[dicor$Term == "moldav"] <- "moldavie"
dicor$Term[dicor$Term == "multisitu"] <- "multisitué"
dicor$Term[dicor$Term == "normand"] <- "normandie"
dicor$Term[dicor$Term == "normat"] <- "normatif"
dicor$Term[dicor$Term == "océan"] <- "océanie"
dicor$Term[dicor$Term == "orphelinag"] <- "orphelinage"
dicor$Term[dicor$Term == "paléodémograph"] <- "paléodémographie"
dicor$Term[dicor$Term == "pari"] <- "paris"
dicor$Term[dicor$Term == "polynes"] <- "polynésie"
dicor$Term[dicor$Term == "quatr"] <- "quatre"
dicor$Term[dicor$Term == "recens"] <- "recensé"
dicor$Term[dicor$Term == "recompos"] <- "recomposé"
dicor$Term[dicor$Term == "reconfigur"] <- "reconfiguration"
dicor$Term[dicor$Term == "remigr"] <- "remigration"
dicor$Term[dicor$Term == "sad"] <- "sade"
dicor$Term[dicor$Term == "saisonnal"] <- "saisonnalité"
dicor$Term[dicor$Term == "sexual"] <- "sexualité"
dicor$Term[dicor$Term == "sociodémograph"] <- "sociodémographique"
dicor$Term[dicor$Term == "socioéconom"] <- "socioéconomique"
dicor$Term[dicor$Term == "sou"] <- "sous"
dicor$Term[dicor$Term == "surmortal"] <- "surmortalité"
dicor$Term[dicor$Term == "teew"] <- "teewa"
dicor$Term[dicor$Term == "thaïland"] <- "thaïlande"
dicor$Term[dicor$Term == "tunis"] <- "tunisie"
dicor$Term[dicor$Term == "trent"] <- "trente"
dicor$Term[dicor$Term == "xvii"] <- "xviie"
dicor$Term[dicor$Term == "xviii"] <- "xviiie"
dicor$Term[dicor$Term == "xix"] <- "xixe"

#concordances(corpus, dtm_t, "uvre")
# correction des fautes d'orthographes
dicor$Term[dicor$Term == "uvre"] <- "oeuvre"
dicor$Term[dicor$Term == "etude"] <- "étude"

```

## Nouveau tableau lexical à partir du dictionnaire personnalisé

La fonction `combine_terms` permet de remplacer les mots initiaux par les *Terms* issus de ce dernier lemmatiseur (`dicor`).

```{r combine}
dtm_t2 <- combine_terms(dtm_t, dicor)
dtm_t2
```

La taille du vocabulaire est *réduite* à 3948 mots distincts au lieu de 4893 initialement.

## Les "mots" les plus employés

On affiche la distibution des 50 mots les plus employés dans les 4247 titres (occurrence et %).

```{r distrib}
frequent_terms(dtm_t2, n=50)
```

*Population* (au singulier ou au pluriel) est utilisé 652 fois dans les titres et représente 2,8% des occurrences, *enquête* (ou enquêtes) 188 fois et près de 0,8% des occurrences .

On voit aussi que nous on pouvons continuer à améliorer la lemmatisation (ex : étatsun mis à la place de etatsunis).
La lecture des concordances nous a montré que xix faisait référence au siècle et nous l'avons gardé.

Le *nuage de mot* permet de visualiser les mots les plus fréquents du corpus des titres.
On affiche ici les mots d'au moins 5 occurrences et 50 mots au maximum.

```{r wordle, eval=FALSE}
set.seed(1)
cloud <- word_cloud(dtm_t2, color="blue", n=50, min.freq=50)
```

![](img/Ch05_nuage_mots.png){fig-align="center"}

## Repérer les cooccurrences

On affiche les mots *cooccurrents*, c'est à dire présents simultanément dans les titres.

### Recherche de mots cooccurrents à certains mots "au choix"

```{r cooc_t}
# Fonction `cooc_terms`
# Mots cooccurents à`Famille` 
cooc_terms(dtm_t2, "famille", n=10)
#cooc_terms(dtm_t2, "vie", n=10)
#cooc_terms(dtm_t2, "mortalité", n=10)
```

Parmi les titres contenant le mot *famille*, *reconstitution* représente 1,1% des occurrences et 61,1% des occurrences de *reconstitution* sont présentes dans les titres contiennent aussi le mot *famille* (avec une valeut test (t value) très forte.

### Autour des mots les plus centraux

On génère un graphe de mots où les mots les plus fréquents sont centraux (analyse de similitude) et où les liens représentent les cooccurrences entre les mots.

```{r graph}
terms_graph(dtm_t2,n= 50,min_occ = 5, interactive = FALSE)
#terms_graph(dtm_t2,n= 50,min_occ = 5, interactive = TRUE)
```

![](img/Ch05_graphe_mots.png){fig-align="center"}

## Mettre en relation mots utilisés dans les titres des publications en français et métadonnées

On utilise les caractéristiques des publications à disposition (métadonnées) selon l'année, la période ou le nombre d'auteurs pour voir s'il y a des spécificités en terme de nombre ou d'emploi de mots.

Pour mémoire nous disposons des métadonnées suivantes :

```{r meta}
head(meta(corpus))
```

Celles-ci sont réparties ainsi :

```{r repart}
# Par période
table(meta(corpus)$Periode)

# Par année de parution (graphique)
ggplot(meta(corpus)) +
  aes(x = Annee) +
  geom_bar(position = "dodge", fill = "#112446") +
  labs(title = "Répartition par Année") +
  coord_flip() +
  theme_minimal()

# par Nombre d'auteurs
table(meta(corpus)$auteurs_nbs)
```

## Bilan lexical

On construit des sous-ensembles de titres selon les modalités des métadonnées (ici période) qu'on compare à l'ensemble.

### Nombre de mots dans les titres

```{r, bilex_moy}
# Fonction `lexical_summary` : calcul de moyennes
lexical_summary(dtm_t2,corpus,"Periode")
```

Les titres des publications les plus récentes contennent en moyenne plus de 7 "mots" différents contre 4 dans la période 1946-1950.

```{r, bilex_nb}
# Fonction `lexical_summary` : calcul d'occurrences
lexical_summary(dtm_t2,corpus,"Periode",unit = "global")
```

Le tableau lexical contient *3848 termes* correspondant à *23148 occurrences* sur les 38492 mots initiaux du corpus.
Dans le corpus des titres publiés entre 2010 et 2020 on dénombre *1512* termes distincts contre *1497* entre 1946 et 1950.

## Calculer les spécificités

Un "*"mot"* est *spécifique* au sous-corpus si sa fréquence y est"anormalement élevée" (test).

### Mots spécifiques

On affiche les mots specifiques de chaque période par ordre de valeur-test décroissante.
Ici on compare les mots utilisés dans les titres selon les décennies.

```{r Specif_T}
specific_terms(dtm_t2,meta(corpus)$Periode, n=5)
```

Ces valeurs sont significatives au seuil de 5% (p-value:0.05 et t-value:2).

Dans notre exemple, *couple* est specifique des titres des articles de la décennie (2010-2020).
Il représente 1,3% (20/1512) des occurrences des "mots" issus des titres de 2010-2020.
Dans 47% (20/42) des cas, *couple* est présent dans les titres de la décennie 2010-2020 et près de 20% des occurrences de ce mot sont datées de cette période.

## Fréquence d'un mot par sous-population

On peut aussi regarder la fréquence d'un mot (ici siècle) par sous-population.

```{r freq_T}
term_freq(dtm_t2, "siècle", meta(corpus)$Periode)
```

Le mot "siècle" est plus employé 199 fois dans les titres et 65 fois pour des articles écrits entre les années 1970 et 1980.

## Analyse des correspondances sur le tableau lexical agrégé

Parmi les méthodes de statistique textuelle, on peut faire une analyse factorielle sur un tableau de contingence croisant les *mots* et les modalités d'une ou plusieurs métadonnées (*tableau lexical agrégé*).

Pour notre premier exemple, on va comparer les mots employées sur 74 années de titres d'articles

```{r AFC_TLA, message=FALSE, warning=FALSE}
# Comparer les années
resTLA_A <- corpus_ca(corpus, dtm_t2, variables=c("Annee"), sparsity=0.98)
```

Les résultats de l'analyse factorielle (plans factoriels, contributions axes par axes) s'affichent dans une fenêtre interactive à l'exécution de la commande `explor()`.
On y retrouve les valeurs des *contributions* axes par axes qui permettent de déterminer les modalités des variables qui différencient le plus les champs lexicaux des titres.
Le signe - ou + des valeurs des *coordonnées* permettent de repérer les oppositions qu'on visualise sur la plan factoriel.

```{r AFC_TLAr, eval=FALSE,  message=FALSE, warning=FALSE}
# explor(resTLA_A)
res <- explor::prepare_results(resTLA_A)
explor::CA_var_plot(res, xax = 1, yax = 2, lev_sup = FALSE, var_sup = FALSE,
    var_sup_choice = , var_hide = "None", var_lab_min_contrib = 7, col_var = "Position",
    symbol_var = "Type", size_var = NULL, size_range = c(10, 300), labels_size = 10,
    point_size = 20, transitions = TRUE, labels_positions = NULL, xlim = c(-1.2,
        0.699), ylim = c(-1.06, 0.84))
```

Affichage du premier plan factoriel

![](img/Ch_05_plan_annee.png){fig-align="center"}

La lecture du plan factoriel nous permet de repérer un champs lexical propre à la première moitié des années 70 (axe 2), des registres opposés dans les titres des années 50 et 60 (situation, problèmes, international) et aussi dans les titres des années 90 à 2020 avec des termes plus "démographiques"(évolution,fécondité, mortalité, mariage).

Au vu de cette répartition des cooccurrences entre les mots des titres étudiés, nous pouvons utiliser la variable Periode.

On choisit ici de contruire le tableau de contingence avec la période de publication et le nombre d'auteurs.

```{r AFC_TLA2, eval=FALSE, message=FALSE, warning=FALSE}
# auteurs_nbs et periode
resTLA_nbAP <- corpus_ca(corpus, dtm_t2, variables=c("auteurs_nbs","Periode"), sparsity=0.98)
# explor(resTLA_nbAP)
```

Affichage du plan factoriel

![](img/Ch_05_plan_per_nbaut_periode.png){fig-align="center"}

```{r AFC_TLA2r, message=FALSE, warning=FALSE, eval=FALSE}
res2 <- explor::prepare_results(resTLA_nbAP)
explor::CA_var_plot(res2, xax = 1, yax = 2, lev_sup = FALSE, var_sup = FALSE,
    var_sup_choice = , var_hide = "None", var_lab_min_contrib = 3, col_var = "Position",
    symbol_var = "Type", size_var = NULL, size_range = c(10, 300), labels_size = 10,
    point_size = 30, transitions = TRUE, labels_positions = NULL, xlim = c(-0.864,
        0.647), ylim = c(-0.795, 0.716))
```

On retrouve l'évolution des champs lexicaux des titres des articles de Population au cours du temps et on note une pratique de co écriture plus fréquente à partir des années 70.
Ce plan factoriel résume plus de 60% de l'informations contenue avec ces données.


## Conclusion de ce chapitre

Ces résultas montrent les analyses possibles en statistique textuelle mais qu'ils nécessitent encore des ajustements sur le vocabulaire à afficher ("etatsun").

La lecture des plans factoriels issus de l'analyse textuellenous montre une synthétise du corpus des mots employés dans les titres scrapés du site de Population.

-   des thèmes plus généralistes aux débuts de la revue : "situation","internationale", "problème",
-   un vocabulaire très liée à la démographie à la fin du XXe siècle avec "mortalité", "fécondité", "mariage",
-   plus des sujets liés à la famille et en "Europe" plus récemment.
