library(RSelenium)
# library(httr)
rD <- rsDriver(port = 4442L, browser="firefox", chromever = NULL)
remDr <- rD[["client"]]

start_url <- "https://quotes.toscrape.com/"
remDr$navigate(start_url)

# quotes_elements <- remDr$findElements("css selector", ".quote")

# quote <- list()
# quote_text <- quotes_elements[[1]]$findChildElement("css selector", ".text")$getElementText()
# # unlist(quote_text)
# quote_author <- quotes_elements[[1]]$findChildElement("css selector", ".author")$getElementText()
# # unlist(quote_author)
# quote_tags_elements <- quotes_elements[[1]]$findChildElements("css selector", ".tag")
# quote_tags <- list(unlist(lapply(quote_tags_elements, function(quote_tag){
#   quote_tag$getElementText()
# })))
# quote['Author'] <- quote_author
# quote['Quote'] <- quote_text
# quote['Tags'] <- quote_tags
# quote

# next_page_element <- remDr$findElements("css", ".next a")
# next_page_url <- unlist(lapply(next_page_element,function(next_page){
#   next_page$getElementAttribute("href")
# }))
# next_page_url

get_next_quotes_page_url <- function() {
  next_button_element <- remDr$findElements("css selector", ".next a")
  next_quotes_page_url <- NULL
  if(length(next_button_element) > 0) {
    next_quotes_page_url <- unlist(lapply(next_button_element, function(next_page){
      next_page$getElementAttribute("href")
    }))
  }
  return(next_quotes_page_url)
}

# quotes_pages <- get_quotes_pages(start_url)

get_quotes_elements <- function(page_url) {
  remDr$navigate(page_url)
  quotes_elements <- remDr$findElements("css selector", ".quote")
  return(quotes_elements)
}

authors_urls <- list()

get_quote <- function(quote_element) {
  quote <- list()
  quote_text <- quote_element$findChildElement("css selector", ".text")$getElementText()
  # unlist(quote_text)
  quote_author <- quote_element$findChildElement("css selector", ".author")$getElementText()
  # unlist(quote_author)
  quote_tags_elements <- quote_element$findChildElements("css selector", ".tag")
  quote_tags <- list(unlist(lapply(quote_tags_elements, function(quote_tag){
    quote_tag$getElementText()
  })))
  quote['author'] <- quote_author
  quote['quote'] <- quote_text
  quote['tags'] <- quote_tags
  
  print("  Searching about element ...")
  
  authors_urls <<- append(authors_urls, get_author_url(quote_element))
  
  
  return(quote)
}

get_author_url <- function(quote_element) {
  author_page_url <- quote_element$findChildElement("css selector", "a[href*='/author/']")$getElementAttribute("href")
  print(paste("  author url found :", author_page_url))
  return(author_page_url)
}

get_author <- function(author_page_url) {
  print(paste("  processing scraping of : ", author_page_url))
  remDr$navigate(author_page_url)
  print(paste("  I am now on page : ", remDr$getCurrentUrl()))
  
  # author <- list()
  # author_name = remDr$findElement("css selector", ".author-title")$getElementText()
  # print(paste("  author name  ", author_name))
  # # author_born_date = html_nodes(author_page,'.author-born-date') %>% html_text()
  # # # print(paste("  Author born date  ", author_born_date))
  # # author_born_location = html_nodes(author_page,'.author-born-location') %>% html_text()
  # # author_description = html_nodes(author_page,'.author-description') %>% html_text()
  # # # print(paste("  Author description  ", author_description))
  # author[['name']] <- author_name
  # # author[['BornDate']] <- author_born_date
  # # author[['BornLocation']] <- author_born_location
  # # author[['Description']] <- author_description
  # # # print("Author scraped : ")
  # # # print(author)
  # return(list(author))
}

# quotes <- lapply(get_quotes_elements(start_url), get_quote)

get_quotes <- function(page_url) {
  print(paste("Processing  ", page_url))
  # current page
  new_quotes <- lapply(get_quotes_elements(page_url), get_quote)
  quotes <<- append(quotes, new_quotes)
  # Find next page
  next_quotes_page_url <- get_next_quotes_page_url()
  if (!is.null(next_quotes_page_url)) {
    print(paste("next page url =  ", next_quotes_page_url))
    remDr$navigate(next_quotes_page_url)
    get_quotes(next_quotes_page_url)
  }
}

get_authors <- function() {
  for (author_url in authors_urls) {
    new_author <- get_author(author_url)
    authors <<- append(authors, new_author)
  }
}

quotes <- list()
authors <- list()
get_quotes(start_url)
authors_urls <- unique(authors_urls)
get_authors()

